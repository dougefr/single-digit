package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.ListUserGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ListUserInteractorTest {
  @Mock private ListUserGateway listUserGateway;
  private ListUserInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor = new ListUserInteractor(listUserGateway);
  }

  @Test
  void shouldThrowAnExceptionIfPassedEmailIsInvalid() {
    try {
      var model = new ListUserInteractor.RequestModel();
      model.setEmail("invalid-email");

      interactor.execute(model);
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("email should be valid", e.getMessage());
    }
  }

  @Test
  void shouldCallFindAllWhenNoFilterWasInformed() {
    try {
      when(listUserGateway.findAll()).thenReturn(Arrays.asList(new User(), new User(), new User()));
      var model = new ListUserInteractor.RequestModel();
      var result = interactor.execute(model);
      assertEquals(3, result.size());
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }

  @Test
  void shouldCallFindByEmailWhenOnlyEmailFilterWasInformed() {
    try {
      when(listUserGateway.findByEmail("dougefr@gmail.com"))
          .thenReturn(Arrays.asList(new User(), new User(), new User(), new User()));

      var model = new ListUserInteractor.RequestModel();
      model.setEmail("dougefr@gmail.com");

      var result = interactor.execute(model);
      assertEquals(4, result.size());
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }

  @Test
  void shouldCallFindByEmailWhenOnlyNameFilterWasInformed() {
    try {
      when(listUserGateway.findByName("Douglas Rodrigues"))
          .thenReturn(Arrays.asList(new User(), new User(), new User(), new User(), new User()));

      var model = new ListUserInteractor.RequestModel();
      model.setName("Douglas Rodrigues");

      var result = interactor.execute(model);
      assertEquals(5, result.size());
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }

  @Test
  void shouldCallFindByEmailAndNameWhenAllFiltersWereInformed() {
    try {
      when(listUserGateway.findByEmailAndName("dougefr@gmail.com", "Douglas Rodrigues"))
          .thenReturn(Arrays.asList(new User(), new User()));

      var model = new ListUserInteractor.RequestModel();
      model.setName("Douglas Rodrigues");
      model.setEmail("dougefr@gmail.com");

      var result = interactor.execute(model);
      assertEquals(2, result.size());
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }
}
