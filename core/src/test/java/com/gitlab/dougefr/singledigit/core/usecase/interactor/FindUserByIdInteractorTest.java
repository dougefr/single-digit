package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.UserNotFoundException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindUserByIdGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class FindUserByIdInteractorTest {
  @Mock private FindUserByIdGateway findUserByIdGateway;
  private FindUserByIdInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor = new FindUserByIdInteractor(findUserByIdGateway);
  }

  @Test
  void shouldThrowAnExceptionIfANullIdWasPassedAsParam() {
    try {
      interactor.execute(null);
    } catch (Exception e) {
      assertEquals("id param cannot be null", e.getMessage());
    }
  }

  @Test
  void shouldThrowAnExceptionIfTheUserWasNotFound() {
    try {
      when(findUserByIdGateway.findById(1L)).thenReturn(Optional.empty());
      interactor.execute(1L);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(UserNotFoundException.class, e.getClass());
    }
  }

  @Test
  void shouldReturnTheFoundUser() {
    try {
      var user = new User();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");
      when(findUserByIdGateway.findById(1L)).thenReturn(Optional.of(user));

      var userFound = interactor.execute(1L);
      assertEquals(user.getId(), userFound.getId());
      assertEquals(user.getName(), userFound.getName());
      assertEquals(user.getEmail(), userFound.getEmail());

    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }
}
