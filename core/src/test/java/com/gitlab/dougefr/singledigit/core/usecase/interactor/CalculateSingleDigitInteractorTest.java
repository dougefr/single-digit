package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindSingleDigitByIdGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.GetSingleDigitFromCacheGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.PutSingleDigitToCacheGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveSingleDigitGateway;
import com.gitlab.dougefr.singledigit.core.usecase.interactor.CalculateSingleDigitInteractor.RequestModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.junit.jupiter.params.provider.NullSource;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CalculateSingleDigitInteractorTest {
  @Mock private FindUserByIdInteractor findUserByIdInteractor;
  @Mock private SaveSingleDigitGateway saveSingleDigitGateway;
  @Mock private GetSingleDigitFromCacheGateway getSingleDigitFromCacheGateway;
  @Mock private PutSingleDigitToCacheGateway putSingleDigitToCacheGateway;
  @Mock private FindSingleDigitByIdGateway findSingleDigitByIdGateway;
  private CalculateSingleDigitInteractor interactor;

  @BeforeEach
  void setup() {
    interactor =
        new CalculateSingleDigitInteractor(
            findUserByIdInteractor,
            saveSingleDigitGateway,
            getSingleDigitFromCacheGateway,
            putSingleDigitToCacheGateway,
            findSingleDigitByIdGateway);
  }

  @ParameterizedTest
  @ValueSource(strings = {"", "abcd", "012a", "            "})
  @NullSource
  void shouldValidateNParam(String n) {
    try {
      interactor.execute(new RequestModel(null, n, 1));
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("n must be a number", e.getMessage());
    }
  }

  @ParameterizedTest
  @ValueSource(ints = {0, -1, -2, -3, Integer.MIN_VALUE})
  @NullSource
  void shouldValidateKParam(Integer k) {
    try {
      interactor.execute(new RequestModel(null, "123", k));
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("k must be a positive number", e.getMessage());
    }
  }

  @ParameterizedTest
  @CsvSource({
    "9875,4,8",
    "123456789,10,9",
    "987654321,10,9",
    "941258745621,7415,9",
    "54576784123,457845,6"
  })
  void shouldCalculateSingleDigitCorrectly(String n, Integer k, Integer result)
      throws BusinessException {
    when(getSingleDigitFromCacheGateway.get(n, k)).thenReturn(null);
    var singleDigit = interactor.execute(new RequestModel(null, n, k));
    assertEquals(n, singleDigit.getN());
    assertEquals(k, singleDigit.getK());
    assertEquals(result, singleDigit.getResult());
  }

  @RepeatedTest(1000)
  void shouldResultsValuesThatAreCached() throws BusinessException {
    var n = String.valueOf(Math.round(Math.random() * 10000) + 1);
    var k = Math.toIntExact(Math.round(Math.random() * 10000) + 1);
    var result = Math.toIntExact(Math.round(Math.random() * 9));

    when(getSingleDigitFromCacheGateway.get(n, k)).thenReturn(result);

    var singleDigit = interactor.execute(new RequestModel(null, n, k));
    assertEquals(n, singleDigit.getN());
    assertEquals(k, singleDigit.getK());
    assertEquals(result, singleDigit.getResult());
  }

  @RepeatedTest(1000)
  void shouldCacheResults() throws BusinessException {
    var n = String.valueOf(Math.round(Math.random() * 10000) + 1);
    var k = Math.toIntExact(Math.round(Math.random() * 10000) + 1);

    when(getSingleDigitFromCacheGateway.get(n, k)).thenReturn(null);
    var valueCapture = ArgumentCaptor.forClass(Integer.class);
    doNothing()
        .when(putSingleDigitToCacheGateway)
        .put(anyString(), anyInt(), valueCapture.capture());

    var result = interactor.execute(new RequestModel(null, n, k));

    verify(putSingleDigitToCacheGateway, times(1)).put(n, k, valueCapture.getValue());
    assertEquals(valueCapture.getValue(), result.getResult());
  }

  @Test
  void shouldAssociateResultToUserWhenItWasInformed() throws BusinessException {
    when(findUserByIdInteractor.execute(1L))
        .thenReturn(
            new User() {
              {
                setId(1L);
              }
            });
    var result = interactor.execute(new RequestModel(1L, "123", 1));
    assertTrue(result.getCalculatedBy().stream().anyMatch(user -> user.getId().equals(1L)));
  }
}
