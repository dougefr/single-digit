package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.EmailAlreadyInUseException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindUserByEmailGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveUserGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class CreateUserInteractorTest {
  @Mock private FindUserByEmailGateway findUserByEmailGateway;
  @Mock private SaveUserGateway saveUserGateway;
  private CreateUserInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor = new CreateUserInteractor(findUserByEmailGateway, saveUserGateway);
  }

  @Test
  void shouldValidateUserBlankName() {
    try {
      var user = new CreateUserInteractor.RequestModel();
      user.setEmail("dougefr@gmail.com");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("name cannot be blank", e.getMessage());
    }
  }

  @Test
  void shouldValidateUserBlankEmail() {
    try {
      var user = new CreateUserInteractor.RequestModel();
      user.setName("Douglas Rodrigues");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("email cannot be blank", e.getMessage());
    }
  }

  @Test
  void shouldValidateUserInvalidEmail() {
    try {
      var user = new CreateUserInteractor.RequestModel();
      user.setName("Douglas Rodrigues");
      user.setEmail("invalid-email");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("email should be valid", e.getMessage());
    }
  }

  @Test
  void shouldThrowAnErrorWhenAlreadyExistsAnUserWithTheEmail() {
    try {
      when(findUserByEmailGateway.findOneByEmail("dougefr@gmail.com"))
          .thenReturn(Optional.of(new User()));

      var user = new CreateUserInteractor.RequestModel();
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(EmailAlreadyInUseException.class, e.getClass());
    }
  }

  @Test
  void shouldResultsTheCreatedUser() {
    try {
      var user = new CreateUserInteractor.RequestModel();
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");

      var expectedUser = new User();
      expectedUser.setName(user.getName());
      expectedUser.setEmail(user.getEmail());

      when(findUserByEmailGateway.findOneByEmail("dougefr@gmail.com")).thenReturn(Optional.empty());
      when(saveUserGateway.save(expectedUser)).thenReturn(expectedUser);

      var userCreated = interactor.execute(user);
      assertEquals(userCreated.getName(), user.getName());
      assertEquals(userCreated.getEmail(), user.getEmail());

    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }
}
