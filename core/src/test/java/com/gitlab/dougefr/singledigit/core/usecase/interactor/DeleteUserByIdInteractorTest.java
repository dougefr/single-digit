package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.DeleteUserGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class DeleteUserByIdInteractorTest {
  @Mock private FindUserByIdInteractor findUserByIdInteractor;
  @Mock private DeleteUserGateway deleteUserGateway;
  private DeleteUserByIdInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor = new DeleteUserByIdInteractor(findUserByIdInteractor, deleteUserGateway);
  }

  @Test
  void shouldThrowBusinessErrorIfFindUserByIdThrowOne() {
    var fakeError = new ValidationException("fake error");

    try {
      when(findUserByIdInteractor.execute(1L)).thenThrow(fakeError);
      interactor.execute(1L);
    } catch (Exception e) {
      assertEquals(fakeError, e);
    }
  }

  @Test
  void shouldReturnNullWhenUserWasDeleted() {
    try {
      when(findUserByIdInteractor.execute(1L)).thenReturn(new User());
      var result = interactor.execute(1L);
      assertNull(result);
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }
}
