package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;
import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindSingleDigitsCalculatedByUserGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ListSingleDigitsCalculatedByUserIdInteractorTest {
  @Mock private FindSingleDigitsCalculatedByUserGateway findSingleDigitsCalculatedByUserGateway;
  @Mock private FindUserByIdInteractor findUserByIdInteractor;
  private ListSingleDigitsCalculatedByUserIdInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor =
        new ListSingleDigitsCalculatedByUserIdInteractor(
            findSingleDigitsCalculatedByUserGateway, findUserByIdInteractor);
  }

  @Test
  void shouldThrowAnExceptionIfANullIdWasPassedAsParam() {
    try {
      interactor.execute(null);
    } catch (Exception e) {
      assertEquals("id param cannot be null", e.getMessage());
    }
  }

  @Test
  void shouldReturnSingleDigitsCalculatedByUser() {
    try {
      var user = new User();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");
      when(findUserByIdInteractor.execute(1L)).thenReturn(user);

      var expectedResult =
          new SingleDigit[] {
            new SingleDigit("1", 2, 3), new SingleDigit("4", 5, 6), new SingleDigit("7", 8, 9),
          };
      when(findSingleDigitsCalculatedByUserGateway.findByCalculatedBy(user))
          .thenReturn(Arrays.asList(expectedResult));

      var result = interactor.execute(1L);
      assertArrayEquals(expectedResult, result.toArray(new SingleDigit[] {}));

    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }
}
