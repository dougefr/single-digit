package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveUserGateway;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class SavePublicRSAKeyInteractorTest {
  @Mock FindUserByIdInteractor findUserByIdInteractor;
  @Mock SaveUserGateway saveUserGateway;
  private SavePublicRSAKeyInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor = new SavePublicRSAKeyInteractor(findUserByIdInteractor, saveUserGateway);
  }

  @Test
  void shouldThrowAnErrorWhenUserIdIsNull() {
    try {
      var requestModel = new SavePublicRSAKeyInteractor.RequestModel();
      requestModel.setRsaPublicKey("any-key");
      interactor.execute(requestModel);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("userId cannot be blank", e.getMessage());
    }
  }

  @Test
  void shouldThrowAnErrorWhenRSAPublicKeyIsNull() {
    try {
      var requestModel = new SavePublicRSAKeyInteractor.RequestModel();
      requestModel.setUserId(1L);
      interactor.execute(requestModel);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("RSA Public Key cannot be blank", e.getMessage());
    }
  }

  @ParameterizedTest
  @ValueSource(
      strings = {
        "invalid-base64",
        "LS0tLS1CRUdJTiBQVUJMSUMgS0VZLS0tLS0KTUlJQklqQU5CZ2txaGtpRzl3MEJBUUVGQUFPQ0FR"
            + "OEFNSUlCQ2dLQ0FRRUF2emxzWmFDa0l2L3hJSGUzL1ZXdQoyNTVNVkJoQ0lSY3FUdWxLcTFHSWVB"
            + "Qm84ZzdHRHhpblVjbng2QUR6SzNBRmIvbldRVXlsQjBVd241eVJGajdCCmNXdmlvUUg5bFdZRGdC"
            + "MURPcFQra3graW43d2pMbHkvOG5kTE5YWG5KdU01YTRvNUxXLzBjTUZYR3JiOEVBQ1UKdUNGZ2N6"
            + "SHB4Z0xTZVVDWjdTeE1pdjJpRFQ2YVZQMTRaUkRpKytqdGhYQmJ2MS9nbFF2N0h1SVJPRkZDSjlZ"
            + "Zwp3ZHlubksyZHdZUnN3emd6eG96WFRWSlN0eGZnZTUzYnFLdG5GUFhUdUdlZU5TblhJTlNBanU4"
            + "aWN3QjZITjk0CmpCdEQyUTdIS0xsbzRoT211amczSFpuVVB4clhEUTFmZkIvY3BLOVRFUnQ5NzBB"
            + "VlJwWkV0UjFkNDhQTnNYMEkKRndJREFRQUIKLS0tLS1FTkQgUFVCTElDIEtFWS0tLS0tCg=="
      })
  void shouldThrowAnErrorWhenRSAPublicKeyIsInvalid(String key) {
    try {
      var requestModel = new SavePublicRSAKeyInteractor.RequestModel();
      requestModel.setUserId(1L);
      requestModel.setRsaPublicKey(key);
      interactor.execute(requestModel);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("invalid RSA Public Key", e.getMessage());
    }
  }

  @Test
  void shouldSaveAValidRSAPublicKey() throws BusinessException {
    var requestModel = new SavePublicRSAKeyInteractor.RequestModel();
    requestModel.setUserId(1L);
    requestModel.setRsaPublicKey(
        "-----BEGIN PUBLIC KEY-----\n"
            + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvzlsZaCkIv/xIHe3/VWu\n"
            + "255MVBhCIRcqTulKq1GIeABo8g7GDxinUcnx6ADzK3AFb/nWQUylB0Uwn5yRFj7B\n"
            + "cWvioQH9lWYDgB1DOpT+kx+in7wjLly/8ndLNXXnJuM5a4o5LW/0cMFXGrb8EACU\n"
            + "uCFgczHpxgLSeUCZ7SxMiv2iDT6aVP14ZRDi++jthXBbv1/glQv7HuIROFFCJ9Yg\n"
            + "wdynnK2dwYRswzgzxozXTVJStxfge53bqKtnFPXTuGeeNSnXINSAju8icwB6HN94\n"
            + "jBtD2Q7HKLlo4hOmujg3HZnUPxrXDQ1ffB/cpK9TERt970AVRpZEtR1d48PNsX0I\n"
            + "FwIDAQAB\n"
            + "-----END PUBLIC KEY-----");
    requestModel.setRsaPublicKey(
        requestModel
            .getRsaPublicKey()
            .replaceAll("\\n", "")
            .replace("-----BEGIN PUBLIC KEY-----", "")
            .replace("-----END PUBLIC KEY-----", ""));

    when(findUserByIdInteractor.execute(1L)).thenReturn(new User());
    interactor.execute(requestModel);
  }
}
