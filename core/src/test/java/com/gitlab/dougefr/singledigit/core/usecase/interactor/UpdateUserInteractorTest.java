package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.EmailAlreadyInUseException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindUserByEmailGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveUserGateway;
import com.gitlab.dougefr.singledigit.core.usecase.interactor.UpdateUserInteractor.RequestModel;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import javax.crypto.Cipher;
import java.security.KeyFactory;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class UpdateUserInteractorTest {
  @Mock private FindUserByEmailGateway findUserByEmailGateway;
  @Mock private SaveUserGateway saveUserGateway;
  @Mock private FindUserByIdInteractor findUserByIdInteractor;
  private UpdateUserInteractor interactor;

  @BeforeEach
  public void setup() {
    interactor =
        new UpdateUserInteractor(findUserByEmailGateway, findUserByIdInteractor, saveUserGateway);
  }

  @Test
  void shouldValidateUserBlankName() {
    try {
      var user = new RequestModel();
      user.setId(1L);
      user.setEmail("dougefr@gmail.com");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("name cannot be blank", e.getMessage());
    }
  }

  @Test
  void shouldValidateUserBlankEmail() {
    try {
      var user = new RequestModel();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("email cannot be blank", e.getMessage());
    }
  }

  @Test
  void shouldValidateUserInvalidEmail() {
    try {
      var user = new RequestModel();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("invalid-email");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("email should be valid", e.getMessage());
    }
  }

  @Test
  void shouldValidateUserInvalidId() {
    try {
      var user = new RequestModel();
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(ValidationException.class, e.getClass());
      assertEquals("id cannot be blank", e.getMessage());
    }
  }

  @Test
  void shouldThrowAnErrorWhenAlreadyExistsAnUserWithTheEmail() {
    try {
      when(findUserByEmailGateway.findOneByEmail("dougefr@gmail.com"))
          .thenReturn(
              Optional.of(
                  new User() {
                    {
                      setId(2L);
                    }
                  }));

      var user = new RequestModel();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");
      interactor.execute(user);
      fail("exception not thrown");
    } catch (Exception e) {
      assertEquals(EmailAlreadyInUseException.class, e.getClass());
    }
  }

  @Test
  void shouldResultsTheUpdatedUser() {
    try {
      var user = new RequestModel();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");

      var userExpected = new User();
      userExpected.setId(user.getId());
      userExpected.setName(user.getName());
      userExpected.setEmail(user.getEmail());

      when(findUserByEmailGateway.findOneByEmail("dougefr@gmail.com")).thenReturn(Optional.empty());
      when(findUserByIdInteractor.execute(1L)).thenReturn(userExpected);
      when(saveUserGateway.save(userExpected)).thenReturn(userExpected);

      var userCreated = interactor.execute(user);
      assertEquals(userCreated.getName(), user.getName());
      assertEquals(userCreated.getEmail(), user.getEmail());
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }

  @Test
  void shouldNotThrowAnErrorWhenTheUserEmailIsNotUpdated() {
    try {
      when(findUserByEmailGateway.findOneByEmail("dougefr@gmail.com"))
          .thenReturn(
              Optional.of(
                  new User() {
                    {
                      setId(1L);
                    }
                  }));

      var user = new RequestModel();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");
      interactor.execute(user);
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }

  @Test
  void shouldEncryptUserAttrsWhenItHasAPublicKey() {
    try {
      var user = new RequestModel();
      user.setId(1L);
      user.setName("Douglas Rodrigues");
      user.setEmail("dougefr@gmail.com");

      var userExpected = new User();
      userExpected.setId(user.getId());
      userExpected.setName(user.getName());
      userExpected.setEmail(user.getEmail());
      userExpected.setRsaPublicKey(
          "-----BEGIN PUBLIC KEY-----\n"
              + "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvzlsZaCkIv/xIHe3/VWu\n"
              + "255MVBhCIRcqTulKq1GIeABo8g7GDxinUcnx6ADzK3AFb/nWQUylB0Uwn5yRFj7B\n"
              + "cWvioQH9lWYDgB1DOpT+kx+in7wjLly/8ndLNXXnJuM5a4o5LW/0cMFXGrb8EACU\n"
              + "uCFgczHpxgLSeUCZ7SxMiv2iDT6aVP14ZRDi++jthXBbv1/glQv7HuIROFFCJ9Yg\n"
              + "wdynnK2dwYRswzgzxozXTVJStxfge53bqKtnFPXTuGeeNSnXINSAju8icwB6HN94\n"
              + "jBtD2Q7HKLlo4hOmujg3HZnUPxrXDQ1ffB/cpK9TERt970AVRpZEtR1d48PNsX0I\n"
              + "FwIDAQAB\n"
              + "-----END PUBLIC KEY-----");
      userExpected.setRsaPublicKey(
          userExpected
              .getRsaPublicKey()
              .replaceAll("\\n", "")
              .replace("-----BEGIN PUBLIC KEY-----", "")
              .replace("-----END PUBLIC KEY-----", ""));

      when(findUserByEmailGateway.findOneByEmail("dougefr@gmail.com")).thenReturn(Optional.empty());
      when(findUserByIdInteractor.execute(1L)).thenReturn(userExpected);
      when(saveUserGateway.save(userExpected)).thenReturn(userExpected);

      var privateKeyContent =
          "-----BEGIN PRIVATE KEY-----\n"
              + "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQC/OWxloKQi//Eg\n"
              + "d7f9Va7bnkxUGEIhFypO6UqrUYh4AGjyDsYPGKdRyfHoAPMrcAVv+dZBTKUHRTCf\n"
              + "nJEWPsFxa+KhAf2VZgOAHUM6lP6TH6KfvCMuXL/yd0s1decm4zlrijktb/RwwVca\n"
              + "tvwQAJS4IWBzMenGAtJ5QJntLEyK/aINPppU/XhlEOL76O2FcFu/X+CVC/se4hE4\n"
              + "UUIn1iDB3KecrZ3BhGzDODPGjNdNUlK3F+B7nduoq2cU9dO4Z541Kdcg1ICO7yJz\n"
              + "AHoc33iMG0PZDscouWjiE6a6ODcdmdQ/GtcNDV98H9ykr1MRG33vQBVGlkS1HV3j\n"
              + "w82xfQgXAgMBAAECggEAVu4i8fxaQbL6ihkCon8uM5a9+kjg3ywpxp/VmxZApNUC\n"
              + "4G+I6Fn7znmwqBiucyPExQfdt+fA6hIxLY4+gs9an1Y9O5atEhrDIp2q71nGdVmf\n"
              + "U2By2eO+OtqybQZBY/NOKi43GgnYJN4v2+wtoJSzBxXdQ+D/9k1YI1kT4LcPwOsN\n"
              + "crRjTzhpUk5uu2yOwU3eg8qhOlkaxwYBR9f5rkcRxcW93R35+cJf0oExnEdIzjhp\n"
              + "1Iiu9bW2tFp3Fqd+/jjbzt4Q+16hId9rllCjBKKfZgmwfvbhWUhbRE7hxC/cafbw\n"
              + "nRpKz+xKhHc4H9N9W/GOmj+/Og7u/MMT4KfHAnAj8QKBgQDdUBrJpS1xocxn18Oe\n"
              + "iYXoXpCFRhm66fHeZC5SCkhzDLrfBCWJIQ2Ahhuj5UE+zlaDLGMeT5HWJO0mXdLa\n"
              + "eFI7l3F9RdeZuTSr8R0RJ3r3XP2z54ZC96vyn4TP6dB37jidlEP2Sl52MmZdlVNR\n"
              + "CAAb8VB7iy1hW5wZI72QUmnFiQKBgQDdMg1yzqDOKDChuSpD0st1auNMy666mVhN\n"
              + "3MLilqbCjfrEgceYsGjRO69f5RGAEBgN4bpOGXsp3pMHMgGPWULdnzDHJChMeQJc\n"
              + "riNuRmpfgzGLTEA9uw+sk6vJek1CWltkMND78hiw9ybZfQWSu0r7MQQdmJu0V43K\n"
              + "hbfB8fKYnwKBgQDG7WXmso4ygZjWDl9LdZnj0HxfDy+5judVWv83xv5wS5T9OSD0\n"
              + "Lb2RNPpLS5pl9dpVdeRyLluL/Q0dW2BX96cS4YGldM3i41Kw6r2R2cbx6agxuX45\n"
              + "W543K6OZrzUvV/WTzt132EkoPQ0+OSb2wxY2B6asJeHYjRu2jSWrQCjK4QKBgQDQ\n"
              + "bkjD5Tcb7D0fN3c/H9P+mbED/W5xgOv3us2dbG5JKDajktM86wZqGv6/sv+RmBjC\n"
              + "g/DlhRdMteH/xrw3ktnyMf5puIWMwf5azPCwCgRUajB2XWyvfqRfb2tR2EATBUfX\n"
              + "GjTjJBbmMo5WJHvq7zfRiTeil9ooztPagbRNOirqawKBgBALbT+LmP+rfYE1lsRU\n"
              + "nHIGBa1yVbJ6YD/12R6EZ5aCyQ/bcnR3PjK4NR7rJgLPcWM/BMH4SC9MvOL8y3ED\n"
              + "TY8j2oCcqSs7uS0ZPftgYw8EvmhToaE7GgiksCMA1hO90+xZMT6e52qYJCz10vbp\n"
              + "d/HA+lTmT2VFzl40rf7HVp82\n"
              + "-----END PRIVATE KEY-----\n";
      privateKeyContent =
          privateKeyContent
              .replaceAll("\\n", "")
              .replace("-----BEGIN PRIVATE KEY-----", "")
              .replace("-----END PRIVATE KEY-----", "");

      var decoder = Base64.getDecoder();
      var keySpecPKCS8 = new PKCS8EncodedKeySpec(decoder.decode(privateKeyContent));
      var pk = KeyFactory.getInstance("RSA").generatePrivate(keySpecPKCS8);

      var decryptCipher = Cipher.getInstance("RSA");
      decryptCipher.init(Cipher.DECRYPT_MODE, pk);

      var userCreated = interactor.execute(user);
      assertEquals(
          user.getName(), new String(decryptCipher.doFinal(decoder.decode(userCreated.getName()))));
      assertEquals(
          user.getEmail(),
          new String(decryptCipher.doFinal(decoder.decode(userCreated.getEmail()))));
    } catch (Exception e) {
      fail("exception was thrown", e);
    }
  }
}
