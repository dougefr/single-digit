package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindSingleDigitsCalculatedByUserGateway;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

@Named
public class ListSingleDigitsCalculatedByUserIdInteractor
    implements BaseInteractor<Long, List<SingleDigit>> {
  private final FindSingleDigitsCalculatedByUserGateway findSingleDigitsCalculatedByUserGateway;
  private final FindUserByIdInteractor findUserByIdInteractor;

  @Inject
  public ListSingleDigitsCalculatedByUserIdInteractor(
      FindSingleDigitsCalculatedByUserGateway findSingleDigitsCalculatedByUserGateway,
      FindUserByIdInteractor findUserByIdInteractor) {
    this.findSingleDigitsCalculatedByUserGateway = findSingleDigitsCalculatedByUserGateway;
    this.findUserByIdInteractor = findUserByIdInteractor;
  }

  @Override
  public List<SingleDigit> execute(Long id) throws BusinessException {
    if (id == null) {
      throw new ValidationException("id param cannot be null");
    }

    return findSingleDigitsCalculatedByUserGateway.findByCalculatedBy(
        findUserByIdInteractor.execute(id));
  }
}
