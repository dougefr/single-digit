package com.gitlab.dougefr.singledigit.core.usecase.igateway;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;
import com.gitlab.dougefr.singledigit.core.entity.User;

import java.util.List;

@FunctionalInterface
public interface FindSingleDigitsCalculatedByUserGateway {
  List<SingleDigit> findByCalculatedBy(User user);
}
