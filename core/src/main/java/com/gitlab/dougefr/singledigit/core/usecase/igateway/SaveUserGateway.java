package com.gitlab.dougefr.singledigit.core.usecase.igateway;

import com.gitlab.dougefr.singledigit.core.entity.User;

@FunctionalInterface
public interface SaveUserGateway {
  User save(User user);
}
