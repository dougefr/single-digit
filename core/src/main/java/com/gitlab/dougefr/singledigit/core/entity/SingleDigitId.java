package com.gitlab.dougefr.singledigit.core.entity;

import java.io.Serializable;

public class SingleDigitId implements Serializable {
  private String n;
  private Integer k;

  public SingleDigitId() {}

  public SingleDigitId(String n, Integer k) {
    this.n = n;
    this.k = k;
  }

  public String getN() {
    return n;
  }

  public void setN(String n) {
    this.n = n;
  }

  public Integer getP() {
    return k;
  }

  public void setP(Integer k) {
    this.k = k;
  }
}
