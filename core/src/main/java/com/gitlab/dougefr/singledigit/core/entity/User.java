package com.gitlab.dougefr.singledigit.core.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotBlank;
import java.util.Objects;

@Entity
public class User implements BaseEntity {
  @Id
  @GeneratedValue
  @Column(name = "id")
  private Long id;

  @NotBlank
  @Column(name = "name", length = 65535)
  private String name;

  @NotBlank
  @Column(name = "email", length = 65535)
  private String email;

  @Column(name = "rsa_public_key", length = 65535)
  private String rsaPublicKey;

  public Long getId() {
    return id;
  }

  public void setId(Long id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getRsaPublicKey() {
    return rsaPublicKey;
  }

  public void setRsaPublicKey(String rsaPublicKey) {
    this.rsaPublicKey = rsaPublicKey;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    var user = (User) o;
    return Objects.equals(id, user.id);
  }

  @Override
  public int hashCode() {
    return Objects.hash(id);
  }
}
