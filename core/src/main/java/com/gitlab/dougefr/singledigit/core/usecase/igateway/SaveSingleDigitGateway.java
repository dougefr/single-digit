package com.gitlab.dougefr.singledigit.core.usecase.igateway;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;

@FunctionalInterface
public interface SaveSingleDigitGateway {
  SingleDigit save(SingleDigit singleDigit);
}
