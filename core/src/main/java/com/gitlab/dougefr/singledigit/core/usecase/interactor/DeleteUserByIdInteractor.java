package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.DeleteUserGateway;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class DeleteUserByIdInteractor implements BaseInteractor<Long, Void> {
  private final FindUserByIdInteractor findUserByIdInteractor;
  private final DeleteUserGateway deleteUserGateway;

  @Inject
  public DeleteUserByIdInteractor(
      FindUserByIdInteractor findUserByIdInteractor, DeleteUserGateway deleteUserGateway) {
    this.findUserByIdInteractor = findUserByIdInteractor;
    this.deleteUserGateway = deleteUserGateway;
  }

  @Override
  public Void execute(Long id) throws BusinessException {
    var user = findUserByIdInteractor.execute(id);
    deleteUserGateway.deleteCascade(user.getId());
    return null;
  }
}
