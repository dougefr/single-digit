package com.gitlab.dougefr.singledigit.core.usecase.shared;

import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;

import javax.validation.Validation;

public final class Validator {
  private static Validator instance;
  private final javax.validation.Validator validator;

  private Validator() {
    var factory = Validation.buildDefaultValidatorFactory();
    validator = factory.getValidator();
  }

  public static Validator getInstance() {
    if (instance == null) {
      instance = new Validator();
    }

    return instance;
  }

  public void validate(Object object) throws ValidationException {
    var violations = validator.validate(object);
    if (!violations.isEmpty()) {
      throw new ValidationException(violations.iterator().next().getMessage());
    }
  }
}
