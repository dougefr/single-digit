package com.gitlab.dougefr.singledigit.core.usecase.igateway;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;
import com.gitlab.dougefr.singledigit.core.entity.SingleDigitId;

import java.util.Optional;

@FunctionalInterface
public interface FindSingleDigitByIdGateway {
  Optional<SingleDigit> findById(SingleDigitId id);
}
