package com.gitlab.dougefr.singledigit.core.usecase.igateway;

import com.gitlab.dougefr.singledigit.core.entity.User;

import java.util.List;

public interface ListUserGateway {
  List<User> findByEmail(String email);

  List<User> findByName(String name);

  List<User> findByEmailAndName(String email, String name);

  List<User> findAll();
}
