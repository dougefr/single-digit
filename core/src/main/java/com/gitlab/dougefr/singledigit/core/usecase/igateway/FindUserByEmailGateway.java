package com.gitlab.dougefr.singledigit.core.usecase.igateway;

import com.gitlab.dougefr.singledigit.core.entity.User;

import java.util.Optional;

@FunctionalInterface
public interface FindUserByEmailGateway {
  Optional<User> findOneByEmail(String email);
}
