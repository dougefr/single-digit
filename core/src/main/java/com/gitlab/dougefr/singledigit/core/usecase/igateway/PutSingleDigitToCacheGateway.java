package com.gitlab.dougefr.singledigit.core.usecase.igateway;

@FunctionalInterface
public interface PutSingleDigitToCacheGateway {
  void put(String n, Integer k, Integer result);
}
