package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;

@FunctionalInterface
public interface BaseInteractor<req, res> {
  res execute(req req) throws BusinessException;
}
