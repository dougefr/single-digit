package com.gitlab.dougefr.singledigit.core.usecase.exception;

public class UserNotFoundException extends BusinessException {
  public UserNotFoundException() {
    super("user not found");
  }
}
