package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.ListUserGateway;
import com.gitlab.dougefr.singledigit.core.usecase.shared.Validator;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Email;
import java.util.List;

@Named
public class ListUserInteractor
    implements BaseInteractor<ListUserInteractor.RequestModel, List<User>> {
  private final ListUserGateway listUserGateway;

  @Inject
  public ListUserInteractor(ListUserGateway listUserGateway) {
    this.listUserGateway = listUserGateway;
  }

  @Override
  public List<User> execute(RequestModel requestModel) throws BusinessException {
    Validator.getInstance().validate(requestModel);

    if (requestModel.getEmail() != null && requestModel.getName() != null) {
      return listUserGateway.findByEmailAndName(requestModel.getEmail(), requestModel.getName());
    }

    if (requestModel.getName() != null) {
      return listUserGateway.findByName(requestModel.getName());
    }

    if (requestModel.getEmail() != null) {
      return listUserGateway.findByEmail(requestModel.getEmail());
    }

    return listUserGateway.findAll();
  }

  public static class RequestModel {
    private String name;

    @Email(message = "email should be valid")
    private String email;

    public RequestModel() {}

    public RequestModel(String name, String email) {
      this.name = name;
      this.email = email;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }
  }
}
