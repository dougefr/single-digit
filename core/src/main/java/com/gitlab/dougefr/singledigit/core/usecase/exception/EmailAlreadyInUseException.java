package com.gitlab.dougefr.singledigit.core.usecase.exception;

public class EmailAlreadyInUseException extends BusinessException {
  public EmailAlreadyInUseException() {
    super("email already in use");
  }
}
