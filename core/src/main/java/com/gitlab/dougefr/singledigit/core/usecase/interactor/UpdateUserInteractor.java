package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.EmailAlreadyInUseException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindUserByEmailGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveUserGateway;
import com.gitlab.dougefr.singledigit.core.usecase.shared.Validator;

import javax.crypto.Cipher;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Named
public class UpdateUserInteractor
    implements BaseInteractor<UpdateUserInteractor.RequestModel, User> {
  private final FindUserByEmailGateway findUserByEmailGateway;
  private final FindUserByIdInteractor findUserByIdInteractor;
  private final SaveUserGateway saveUserGateway;

  @Inject
  public UpdateUserInteractor(
      FindUserByEmailGateway findUserByEmailGateway,
      FindUserByIdInteractor findUserByIdInteractor,
      SaveUserGateway saveUserGateway) {
    this.findUserByEmailGateway = findUserByEmailGateway;
    this.findUserByIdInteractor = findUserByIdInteractor;
    this.saveUserGateway = saveUserGateway;
  }

  @Override
  public User execute(RequestModel requestModel) throws BusinessException {
    var userFound = validate(requestModel);
    userFound.setName(requestModel.getName());
    userFound.setEmail(requestModel.getEmail());
    encryptAttrs(userFound);

    return saveUserGateway.save(userFound);
  }

  private User validate(RequestModel user) throws BusinessException {
    Validator.getInstance().validate(user);

    var op = findUserByEmailGateway.findOneByEmail(user.getEmail());
    if (op.isPresent()) {
      var userFound = op.get();
      if (!userFound.getId().equals(user.getId())) {
        throw new EmailAlreadyInUseException();
      }

      return userFound;
    }

    return findUserByIdInteractor.execute(user.getId());
  }

  private void encryptAttrs(User user) {
    var rsaPublicKey = user.getRsaPublicKey();
    if (rsaPublicKey == null || rsaPublicKey.equals("")) {
      return;
    }

    try {
      var byteKey = Base64.getDecoder().decode(rsaPublicKey);
      var X509publicKey = new X509EncodedKeySpec(byteKey, "RSA");
      var publicKey = KeyFactory.getInstance("RSA").generatePublic(X509publicKey);

      var encryptCipher = Cipher.getInstance("RSA");
      encryptCipher.init(Cipher.ENCRYPT_MODE, publicKey);

      var encoder = Base64.getEncoder();
      user.setName(encoder.encodeToString(encryptCipher.doFinal(user.getName().getBytes())));
      user.setEmail(encoder.encodeToString(encryptCipher.doFinal(user.getEmail().getBytes())));
    } catch (Exception e) {
      throw new RuntimeException("unexpected error", e);
    }
  }

  public static class RequestModel {
    @NotNull(message = "id cannot be blank")
    private Long id;

    @NotBlank(message = "name cannot be blank")
    private String name;

    @NotBlank(message = "email cannot be blank")
    @Email(message = "email should be valid")
    private String email;

    public Long getId() {
      return id;
    }

    public void setId(Long id) {
      this.id = id;
    }

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }
  }
}
