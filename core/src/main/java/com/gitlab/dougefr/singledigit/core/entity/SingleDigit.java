package com.gitlab.dougefr.singledigit.core.entity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Objects;
import java.util.Set;

@Entity
@IdClass(SingleDigitId.class)
public class SingleDigit implements BaseEntity {
  @Id
  @NotNull
  @Column(name = "n")
  private String n;

  @Id
  @NotNull
  @Column(name = "k")
  private Integer k;

  @NotNull
  @Column(name = "result")
  private Integer result;

  @ManyToMany(cascade = CascadeType.ALL)
  @JoinTable(
      name = "calculated_by",
      joinColumns = {@JoinColumn(name = "n"), @JoinColumn(name = "p")},
      inverseJoinColumns = @JoinColumn(name = "user_id"))
  private Set<User> calculatedBy;

  public SingleDigit() {}

  public SingleDigit(@NotNull String n, @NotNull Integer k, @NotNull Integer result) {
    this.n = n;
    this.k = k;
    this.result = result;
  }

  public String getN() {
    return n;
  }

  public void setN(String n) {
    this.n = n;
  }

  public Integer getK() {
    return k;
  }

  public void setK(Integer k) {
    this.k = k;
  }

  public Integer getResult() {
    return result;
  }

  public void setResult(Integer result) {
    this.result = result;
  }

  public Set<User> getCalculatedBy() {
    return calculatedBy;
  }

  public void setCalculatedBy(Set<User> calculatedBy) {
    this.calculatedBy = calculatedBy;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    var that = (SingleDigit) o;
    return Objects.equals(n, that.n) && Objects.equals(k, that.k);
  }

  @Override
  public int hashCode() {
    return Objects.hash(n, k);
  }
}
