package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.UserNotFoundException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindUserByIdGateway;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class FindUserByIdInteractor implements BaseInteractor<Long, User> {
  private final FindUserByIdGateway findUserByIdGateway;

  @Inject
  public FindUserByIdInteractor(FindUserByIdGateway findUserByIdGateway) {
    this.findUserByIdGateway = findUserByIdGateway;
  }

  @Override
  public User execute(Long id) throws BusinessException {
    if (id == null) {
      throw new ValidationException("id param cannot be null");
    }

    return findUserByIdGateway.findById(id).orElseThrow(UserNotFoundException::new);
  }
}
