package com.gitlab.dougefr.singledigit.core.entity;

import java.io.Serializable;

public interface BaseEntity extends Serializable {}
