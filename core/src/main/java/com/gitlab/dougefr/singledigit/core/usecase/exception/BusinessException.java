package com.gitlab.dougefr.singledigit.core.usecase.exception;

public abstract class BusinessException extends Exception {
  public BusinessException(String message) {
    super(message);
  }
}
