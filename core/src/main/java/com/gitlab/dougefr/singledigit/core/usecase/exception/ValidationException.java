package com.gitlab.dougefr.singledigit.core.usecase.exception;

public class ValidationException extends BusinessException {
  public ValidationException(String message) {
    super(message);
  }
}
