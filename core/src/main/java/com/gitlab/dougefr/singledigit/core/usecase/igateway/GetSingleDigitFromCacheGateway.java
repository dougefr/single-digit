package com.gitlab.dougefr.singledigit.core.usecase.igateway;

@FunctionalInterface
public interface GetSingleDigitFromCacheGateway {
  Integer get(String n, Integer k);
}
