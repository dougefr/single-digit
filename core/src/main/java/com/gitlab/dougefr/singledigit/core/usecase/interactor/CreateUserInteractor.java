package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.EmailAlreadyInUseException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindUserByEmailGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveUserGateway;
import com.gitlab.dougefr.singledigit.core.usecase.shared.Validator;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Named
public class CreateUserInteractor
    implements BaseInteractor<CreateUserInteractor.RequestModel, User> {
  private final FindUserByEmailGateway findUserByEmailGateway;
  private final SaveUserGateway saveUserGateway;

  @Inject
  public CreateUserInteractor(
      FindUserByEmailGateway findUserByEmailGateway, SaveUserGateway saveUserGateway) {
    this.findUserByEmailGateway = findUserByEmailGateway;
    this.saveUserGateway = saveUserGateway;
  }

  @Override
  public User execute(RequestModel requestModel) throws BusinessException {
    validate(requestModel);

    var user = new User();
    user.setName(requestModel.getName());
    user.setEmail(requestModel.getEmail());
    return saveUserGateway.save(user);
  }

  private void validate(RequestModel user) throws BusinessException {
    Validator.getInstance().validate(user);

    if (findUserByEmailGateway.findOneByEmail(user.getEmail()).isPresent()) {
      throw new EmailAlreadyInUseException();
    }
  }

  public static class RequestModel {
    @NotBlank(message = "name cannot be blank")
    private String name;

    @NotBlank(message = "email cannot be blank")
    @Email(message = "email should be valid")
    private String email;

    public String getName() {
      return name;
    }

    public void setName(String name) {
      this.name = name;
    }

    public String getEmail() {
      return email;
    }

    public void setEmail(String email) {
      this.email = email;
    }
  }
}
