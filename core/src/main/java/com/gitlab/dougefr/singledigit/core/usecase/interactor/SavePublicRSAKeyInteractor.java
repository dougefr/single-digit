package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.ValidationException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveUserGateway;
import com.gitlab.dougefr.singledigit.core.usecase.shared.Validator;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.security.KeyFactory;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;

@Named
public class SavePublicRSAKeyInteractor
    implements BaseInteractor<SavePublicRSAKeyInteractor.RequestModel, Void> {
  private final FindUserByIdInteractor findUserByIdInteractor;
  private final SaveUserGateway saveUserGateway;

  @Inject
  public SavePublicRSAKeyInteractor(
      FindUserByIdInteractor findUserByIdInteractor, SaveUserGateway saveUserGateway) {
    this.findUserByIdInteractor = findUserByIdInteractor;
    this.saveUserGateway = saveUserGateway;
  }

  @Override
  public Void execute(RequestModel requestModel) throws BusinessException {
    Validator.getInstance().validate(requestModel);

    try {
      var byteKey = Base64.getDecoder().decode(requestModel.getRsaPublicKey());
      var X509publicKey = new X509EncodedKeySpec(byteKey, "RSA");
      KeyFactory.getInstance("RSA").generatePublic(X509publicKey);
    } catch (Exception e) {
      throw new ValidationException("invalid RSA Public Key");
    }

    var userStored = findUserByIdInteractor.execute(requestModel.getUserId());
    userStored.setRsaPublicKey(requestModel.getRsaPublicKey());
    saveUserGateway.save(userStored);

    return null;
  }

  public static class RequestModel {
    @NotNull(message = "userId cannot be blank")
    private Long userId;

    @NotBlank(message = "RSA Public Key cannot be blank")
    private String rsaPublicKey;

    public Long getUserId() {
      return userId;
    }

    public void setUserId(Long userId) {
      this.userId = userId;
    }

    public String getRsaPublicKey() {
      return rsaPublicKey;
    }

    public void setRsaPublicKey(String rsaPublicKey) {
      this.rsaPublicKey = rsaPublicKey;
    }
  }
}
