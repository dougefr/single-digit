package com.gitlab.dougefr.singledigit.core.usecase.igateway;

@FunctionalInterface
public interface DeleteUserGateway {
  void deleteCascade(Long userId);
}
