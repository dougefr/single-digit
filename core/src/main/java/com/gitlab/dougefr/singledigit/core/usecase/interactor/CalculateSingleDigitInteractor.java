package com.gitlab.dougefr.singledigit.core.usecase.interactor;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;
import com.gitlab.dougefr.singledigit.core.entity.SingleDigitId;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindSingleDigitByIdGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.GetSingleDigitFromCacheGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.PutSingleDigitToCacheGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveSingleDigitGateway;
import com.gitlab.dougefr.singledigit.core.usecase.shared.Validator;

import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Positive;
import java.util.HashSet;

@Named
public class CalculateSingleDigitInteractor
    implements BaseInteractor<CalculateSingleDigitInteractor.RequestModel, SingleDigit> {
  private final FindUserByIdInteractor findUserByIdInteractor;
  private final SaveSingleDigitGateway saveSingleDigitGateway;
  private final GetSingleDigitFromCacheGateway getSingleDigitFromCacheGateway;
  private final PutSingleDigitToCacheGateway putSingleDigitToCacheGateway;
  private final FindSingleDigitByIdGateway findSingleDigitByIdGateway;

  @Inject
  public CalculateSingleDigitInteractor(
      FindUserByIdInteractor findUserByIdInteractor,
      SaveSingleDigitGateway saveSingleDigitGateway,
      GetSingleDigitFromCacheGateway getSingleDigitFromCacheGateway,
      PutSingleDigitToCacheGateway putSingleDigitToCacheGateway,
      FindSingleDigitByIdGateway findSingleDigitByIdGateway) {
    this.findUserByIdInteractor = findUserByIdInteractor;
    this.saveSingleDigitGateway = saveSingleDigitGateway;
    this.getSingleDigitFromCacheGateway = getSingleDigitFromCacheGateway;
    this.putSingleDigitToCacheGateway = putSingleDigitToCacheGateway;
    this.findSingleDigitByIdGateway = findSingleDigitByIdGateway;
  }

  @Override
  public SingleDigit execute(RequestModel requestModel) throws BusinessException {
    Validator.getInstance().validate(requestModel);

    var result = getSingleDigitFromCacheGateway.get(requestModel.getN(), requestModel.getK());
    if (result == null) {
      var resultL = sumRecursively(requestModel.getN()) * requestModel.getK();
      while (resultL > 9) {
        resultL = sumRecursively(Long.toString(resultL));
      }

      result = Math.toIntExact(resultL);
      putSingleDigitToCacheGateway.put(requestModel.getN(), requestModel.getK(), result);
    }

    var singleDigit = new SingleDigit();
    singleDigit.setN(requestModel.getN());
    singleDigit.setK(requestModel.getK());
    singleDigit.setResult(result);

    if (requestModel.getUserId() != null) {
      singleDigit.setCalculatedBy(new HashSet<>());
      singleDigit =
          findSingleDigitByIdGateway
              .findById(new SingleDigitId(singleDigit.getN(), singleDigit.getK()))
              .orElse(singleDigit);

      var user = findUserByIdInteractor.execute(requestModel.getUserId());
      singleDigit.getCalculatedBy().add(user);

      saveSingleDigitGateway.save(singleDigit);
    }

    return singleDigit;
  }

  private Long sumRecursively(String n) {
    if (n.length() == 1) {
      return Long.parseLong(n);
    }

    var headTail = n.split("", 2);
    return Long.parseLong(headTail[0]) + sumRecursively(headTail[1]);
  }

  public static class RequestModel {
    private Long userId;

    @NotNull(message = "n must be a number")
    @Pattern(regexp = "\\d+", message = "n must be a number")
    private String n;

    @NotNull(message = "k must be a positive number")
    @Positive(message = "k must be a positive number")
    private Integer k;

    public RequestModel(Long userId, String n, Integer k) {
      this.userId = userId;
      this.n = n;
      this.k = k;
    }

    public Long getUserId() {
      return userId;
    }

    public void setUserId(Long userId) {
      this.userId = userId;
    }

    public String getN() {
      return n;
    }

    public void setN(String n) {
      this.n = n;
    }

    public Integer getK() {
      return k;
    }

    public void setK(Integer k) {
      this.k = k;
    }
  }
}
