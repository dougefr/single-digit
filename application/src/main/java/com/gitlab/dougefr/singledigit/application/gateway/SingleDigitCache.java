package com.gitlab.dougefr.singledigit.application.gateway;

import com.gitlab.dougefr.singledigit.application.lib.Cache;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.GetSingleDigitFromCacheGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.PutSingleDigitToCacheGateway;
import org.springframework.stereotype.Component;

import java.util.Objects;

@Component
public class SingleDigitCache extends Cache<SingleDigitCache.Key, Integer>
    implements GetSingleDigitFromCacheGateway, PutSingleDigitToCacheGateway {
  public SingleDigitCache() {
    super(10);
  }

  @Override
  public Integer get(String n, Integer k) {
    return get(new Key(n, k));
  }

  @Override
  public void put(String n, Integer k, Integer result) {
    put(new Key(n, k), result);
  }

  protected static class Key {
    private final String n;
    private final Integer k;

    public Key(String n, Integer k) {
      this.n = n;
      this.k = k;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;
      Key key = (Key) o;
      return Objects.equals(n, key.n) && Objects.equals(k, key.k);
    }

    @Override
    public int hashCode() {
      return Objects.hash(n, k);
    }
  }
}
