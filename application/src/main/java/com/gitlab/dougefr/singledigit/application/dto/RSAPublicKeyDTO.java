package com.gitlab.dougefr.singledigit.application.dto;

public class RSAPublicKeyDTO {
  private Long userId;
  private String rsaPublicKey;

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }

  public String getRsaPublicKey() {
    return rsaPublicKey;
  }

  public void setRsaPublicKey(String rsaPublicKey) {
    this.rsaPublicKey = rsaPublicKey;
  }
}
