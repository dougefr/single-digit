package com.gitlab.dougefr.singledigit.application.gateway;

import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository
    extends JpaRepository<User, Long>,
        DeleteUserGateway,
        FindUserByIdGateway,
        FindUserByEmailGateway,
        ListUserGateway,
        SaveUserGateway {
  @Modifying
  @Query(
      value =
          "DELETE FROM calculated_by WHERE user_id = ?1 ; DELETE FROM user WHERE user.id = ?1 ;",
      nativeQuery = true)
  void deleteCascade(Long userId);
}
