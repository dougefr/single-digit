package com.gitlab.dougefr.singledigit.application.controller;

import com.gitlab.dougefr.singledigit.application.dto.RSAPublicKeyDTO;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.interactor.SavePublicRSAKeyInteractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/public-keys")
public class RSAPublicKeyController {
  @Autowired private SavePublicRSAKeyInteractor savePublicRSAKeyInteractor;

  @Transactional
  @PutMapping
  public void updateKey(@RequestBody RSAPublicKeyDTO dto) throws BusinessException {
    var user = new SavePublicRSAKeyInteractor.RequestModel();
    user.setUserId(dto.getUserId());
    user.setRsaPublicKey(dto.getRsaPublicKey());
    savePublicRSAKeyInteractor.execute(user);
  }
}
