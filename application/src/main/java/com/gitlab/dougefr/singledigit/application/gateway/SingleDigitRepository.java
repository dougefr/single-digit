package com.gitlab.dougefr.singledigit.application.gateway;

import com.gitlab.dougefr.singledigit.core.entity.SingleDigit;
import com.gitlab.dougefr.singledigit.core.entity.SingleDigitId;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindSingleDigitByIdGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.FindSingleDigitsCalculatedByUserGateway;
import com.gitlab.dougefr.singledigit.core.usecase.igateway.SaveSingleDigitGateway;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SingleDigitRepository
    extends JpaRepository<SingleDigit, SingleDigitId>,
        SaveSingleDigitGateway,
        FindSingleDigitsCalculatedByUserGateway,
        FindSingleDigitByIdGateway {}
