package com.gitlab.dougefr.singledigit.application.dto;

public class CalculateSingleDigitResponseDTO {
  private Integer n;
  private Integer k;
  private Integer result;

  public CalculateSingleDigitResponseDTO(Integer n, Integer k, Integer result) {
    this.n = n;
    this.k = k;
    this.result = result;
  }

  public Integer getN() {
    return n;
  }

  public void setN(Integer n) {
    this.n = n;
  }

  public Integer getK() {
    return k;
  }

  public void setK(Integer k) {
    this.k = k;
  }

  public Integer getResult() {
    return result;
  }

  public void setResult(Integer result) {
    this.result = result;
  }
}
