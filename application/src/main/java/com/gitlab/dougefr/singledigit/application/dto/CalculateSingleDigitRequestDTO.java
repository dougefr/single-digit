package com.gitlab.dougefr.singledigit.application.dto;

public class CalculateSingleDigitRequestDTO {
  private Integer n;
  private Integer k;
  private Long userId;

  public Integer getN() {
    return n;
  }

  public void setN(Integer n) {
    this.n = n;
  }

  public Integer getK() {
    return k;
  }

  public void setK(Integer k) {
    this.k = k;
  }

  public Long getUserId() {
    return userId;
  }

  public void setUserId(Long userId) {
    this.userId = userId;
  }
}
