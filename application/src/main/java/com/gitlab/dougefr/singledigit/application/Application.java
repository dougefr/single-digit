package com.gitlab.dougefr.singledigit.application;

import com.gitlab.dougefr.singledigit.core.entity.User;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;

@SpringBootApplication(scanBasePackages = "com.gitlab.dougefr.singledigit")
@EntityScan(basePackageClasses = User.class)
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
