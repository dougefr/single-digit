package com.gitlab.dougefr.singledigit.application.lib;

import java.util.LinkedList;
import java.util.Objects;

public class Cache<K, V> {
  private final LinkedList<Item<K, V>> queue;
  private final int size;

  public Cache(int size) {
    this.size = size;
    queue = new LinkedList<>();
  }

  public synchronized void put(K key, V value) {
    if (queue.size() == size) {
      queue.poll();
    }
    queue.offer(new Item<>(key, value));
  }

  public synchronized V get(K key) {
    var item = queue.stream().filter(i -> Objects.equals(i.getKey(), key)).findFirst().orElse(null);
    return item == null ? null : item.getValue();
  }

  public static class Item<K, V> {
    private final K key;
    private final V value;

    public Item(K key, V value) {
      this.key = key;
      this.value = value;
    }

    public K getKey() {
      return key;
    }

    public V getValue() {
      return value;
    }
  }
}
