package com.gitlab.dougefr.singledigit.application.controller;

import com.gitlab.dougefr.singledigit.application.dto.CreateUserRequestDTO;
import com.gitlab.dougefr.singledigit.application.dto.UpdateUserRequestDTO;
import com.gitlab.dougefr.singledigit.application.dto.UserResponseDTO;
import com.gitlab.dougefr.singledigit.core.entity.User;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.interactor.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(path = "/users")
public class UserController {
  @Autowired private ListUserInteractor listUserInteractor;
  @Autowired private FindUserByIdInteractor findUserByIdInteractor;
  @Autowired private CreateUserInteractor createUserInteractor;
  @Autowired private UpdateUserInteractor updateUserInteractor;
  @Autowired private DeleteUserByIdInteractor deleteUserByIdInteractor;

  @GetMapping
  public List<UserResponseDTO> listUser(
      @RequestParam(value = "name", required = false) String name,
      @RequestParam(value = "email", required = false) String email)
      throws BusinessException {
    return listUserInteractor.execute(new ListUserInteractor.RequestModel(name, email)).stream()
        .map(this::userToResponseDTO)
        .collect(Collectors.toList());
  }

  @GetMapping("/{id}")
  public UserResponseDTO getUser(@PathVariable Long id) throws BusinessException {
    return userToResponseDTO(findUserByIdInteractor.execute(id));
  }

  @Transactional
  @PostMapping
  @ResponseStatus(code = HttpStatus.CREATED)
  public UserResponseDTO createUser(@RequestBody CreateUserRequestDTO dto)
      throws BusinessException {
    var user = new CreateUserInteractor.RequestModel();
    user.setName(dto.getName());
    user.setEmail(dto.getEmail());
    return userToResponseDTO(createUserInteractor.execute(user));
  }

  @Transactional
  @PutMapping
  public UserResponseDTO updateUser(@RequestBody UpdateUserRequestDTO dto)
      throws BusinessException {
    var user = new UpdateUserInteractor.RequestModel();
    user.setId(dto.getId());
    user.setName(dto.getName());
    user.setEmail(dto.getEmail());
    return userToResponseDTO(updateUserInteractor.execute(user));
  }

  @Transactional
  @DeleteMapping("/{id}")
  public void deleteUser(@PathVariable Long id) throws BusinessException {
    deleteUserByIdInteractor.execute(id);
  }

  private UserResponseDTO userToResponseDTO(User user) {
    var dto = new UserResponseDTO();
    dto.setId(user.getId());
    dto.setName(user.getName());
    dto.setEmail(user.getEmail());

    return dto;
  }
}
