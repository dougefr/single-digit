package com.gitlab.dougefr.singledigit.application.controller;

import com.gitlab.dougefr.singledigit.application.dto.CalculateSingleDigitRequestDTO;
import com.gitlab.dougefr.singledigit.application.dto.CalculateSingleDigitResponseDTO;
import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.interactor.CalculateSingleDigitInteractor;
import com.gitlab.dougefr.singledigit.core.usecase.interactor.ListSingleDigitsCalculatedByUserIdInteractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/single-digits")
public class SingleDigitController {
  @Autowired private CalculateSingleDigitInteractor calculateSingleDigitInteractor;

  @Autowired
  private ListSingleDigitsCalculatedByUserIdInteractor listSingleDigitsCalculatedByUserIdInteractor;

  @GetMapping
  public List<CalculateSingleDigitResponseDTO> listCalculatedBy(@RequestParam("userId") Long userId)
      throws BusinessException {
    return listSingleDigitsCalculatedByUserIdInteractor.execute(userId).stream()
        .map(
            c ->
                new CalculateSingleDigitResponseDTO(
                    Integer.parseInt(c.getN()), c.getK(), c.getResult()))
        .collect(Collectors.toList());
  }

  @Transactional
  @PostMapping
  public Integer calculate(@RequestBody CalculateSingleDigitRequestDTO dto)
      throws BusinessException {
    return calculateSingleDigitInteractor
        .execute(
            new CalculateSingleDigitInteractor.RequestModel(
                dto.getUserId(), Objects.toString(dto.getN()), dto.getK()))
        .getResult();
  }
}
