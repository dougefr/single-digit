package com.gitlab.dougefr.singledigit.application.configuration;

import com.gitlab.dougefr.singledigit.core.usecase.exception.BusinessException;
import com.gitlab.dougefr.singledigit.core.usecase.exception.UserNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class BusinessExceptionHandler extends ResponseEntityExceptionHandler {
  @ExceptionHandler(BusinessException.class)
  protected ResponseEntity<Object> handle(BusinessException be, WebRequest request) {
    if (be instanceof UserNotFoundException) {
      return handleExceptionInternal(
          be, be.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    return handleExceptionInternal(
        be, be.getMessage(), new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
  }
}
