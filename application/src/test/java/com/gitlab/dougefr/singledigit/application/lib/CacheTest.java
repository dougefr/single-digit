package com.gitlab.dougefr.singledigit.application.lib;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

public class CacheTest {
  @Test
  void shouldAddOneItemToCache() {
    var cache = new Cache<Integer, Integer>(10);
    cache.put(1, 1);
    assertEquals(1, cache.get(1));
  }

  @Test
  void shouldDropFirstEntryWhenCacheIsFull() {
    var cache = new Cache<Integer, Integer>(10);
    for (var i = 0; i < 20; i++) {
      cache.put(i, i);
    }

    for (var i = 0; i < 10; i++) {
      assertNull(cache.get(i));
    }
    for (var i = 10; i < 20; i++) {
      assertEquals(i, cache.get(i));
    }
  }
}
