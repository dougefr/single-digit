### Single-Digit API

#### Execução

* Como compilar e executar a aplicação:

`mvn spring-boot:run`

* Como executar os testes unitários:

`mvn test`

* Documentação da API:

`http://localhost:8080/swagger-ui.html`

#### Observações

O projeto foi estruturado inspirado no Clean Architecture. Sendo assim:
* O módulo `core` contém todo o `domain` da aplicação, correspondendo as
camadas `entity` e `usecase` do modelo padrão da arquitetura. É alheio ao
framework usado nas camadas acima, e suas únicas dependências são `annotations`
referente aos padrões `JSR-330` (injeção de dependência), `JSR 303` (validação)
e `JSR-338` (persistência).
* O módulo `application` corresponde as camadas `interface` e `frameworks` e
contém todo o uso do Spring Boot.